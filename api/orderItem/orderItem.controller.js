const {
    postOrderItem, getOrderItemById, removeOrderItemById
  } = require("./orderItem.service");
  
  
  module.exports = {
    postOrderItem: (req, res) => {
      const body = req.body;
      postOrderItem(body, (err, results) => {
        if (err) {
          console.log(err);
          return;
        }
        return res.json({
          success: 1,
          message: "Order Item created successfully"
        });
      });
    },
    getOrderItemById: (req, res) => {
      const id = req.params.id;
      getOrderItemById(id, (err, results) => {
        if (err) {
          console.log(err);
          return;
        }
        return res.json({
          success: 1,
          data: results
        });
      });
    },
    removeOrderItemById: (req, res) => {
      const id = [req.params.order_id, req.params.item_id]; 
      removeOrderItemById(id, (err, results) => {
        if (err) {
          console.log(err);
          return;
        }
        return res.json({
          success: 1,
          data: results
        });
      });
    }
  };
  