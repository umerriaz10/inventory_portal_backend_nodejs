const pool = require("../../config/database");

module.exports = {
  postOrderItem: (data, callBack) => {
    pool.query(
        `insert into orders_items(order_id, item_id, item_name, quantity, item_price, total_amount) 
                  VALUES ?`,
        [data],
        (error, results, fields) => {
          if (error) {
            callBack(error);
          }
          return callBack(null, results);
        }
      );
  },
  getOrderItemById: (id, callBack) => {
    pool.query(
      `select order_id,item_id,item_name,quantity,item_price,total_amount from orders_items where order_id = ?`,
      [id],
      (error, results, fields) => {
        if (error) {
          callBack(error);
        }
        return callBack(null, results);
      }
    );
  },
  removeOrderItemById: (id, callBack) => {
    pool.query(
      `delete from orders_items where order_id = ? AND item_id = ?`,
      [id[0],
        id[1]],
      (error, results, fields) => {
        if (error) {
          callBack(error);
        }
        return callBack(null, results);
      }
    );
  }
};
