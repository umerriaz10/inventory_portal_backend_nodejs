const router = require("express").Router();
const { checkToken } = require("../../auth/token_validation");
const {
  postOrderItem, getOrderItemById, removeOrderItemById
} = require("./orderItem.controller");

router.post("/", checkToken, postOrderItem);
router.get("/:id", checkToken, getOrderItemById);
router.delete("/:order_id/:item_id", checkToken, removeOrderItemById);

module.exports = router;