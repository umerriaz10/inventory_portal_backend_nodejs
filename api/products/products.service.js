const pool = require("../../config/database");

module.exports = {
  postProduct: (data, callBack) => {
    pool.query(
        `insert into products(p_name, p_quantity, p_price, p_addedBy) 
                  values(?,?,?,?)`,
        [
          data.p_name,
          data.p_quantity,
          data.p_price,
          data.p_addedBy
        ],
        (error, results, fields) => {
          if (error) {
            callBack(error);
          }
          return callBack(null, results);
        }
      );
  },
  removeProductById: (id, callBack) => {
    pool.query(
      `delete from products where id = ?`,
      [id],
      (error, results, fields) => {
        if (error) {
          callBack(error);
        }
        return callBack(null, results);
      }
    );
  },
  getProductByProductId: (id, callBack) => {
    pool.query(
      `select id,p_name,p_quantity,p_price,p_addedBy from products where id = ?`,
      [id],
      (error, results, fields) => {
        if (error) {
          callBack(error);
        }
        return callBack(null, results[0]);
      }
    );
  },
  getProducts: callBack => {
    pool.query(
      `select id,p_name,p_quantity,p_price,p_addedBy from products`,
      [],
      (error, results, fields) => {
        if (error) {
          callBack(error);
        }
        return callBack(null, results);
      }
    );
  },
  updateProductQuantity: (data, callBack) => {
    var sign = '';
    if (data.status == 'Added') {
      sign = '+';
    } else {
      sign = '-';
    }
    pool.query(
      `update products set p_quantity = p_quantity ` + sign + `? where id = ?`,
      [
        data.p_quantity,
        data.id
      ],
      (error, results, fields) => {
        if (error) {
          callBack(error);
        }
        return callBack(null, results[0]);
      }
    );
  },
  updateProduct: (data, callBack) => {
    pool.query(
      `update products set
        p_name = ?, p_quantity = ?,p_price =? where id = ?`,
      [
        data.p_name,
        data.p_quantity,
        data.p_price,
        data.id
      ],
      (error, results, fields) => {
        if (error) {
          callBack(error);
        }
        return callBack(null, results[0]);
      }
    );
  }
};
