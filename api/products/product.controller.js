const {
    getProductByProductId,
    getProducts,
    updateProduct,
    updateProductQuantity,
    postProduct,
    removeProductById
  } = require("./products.service");
  
  
  module.exports = {
    postProduct: (req, res) => {
      const body = req.body;
      postProduct(body, (err, results) => {
        if (err) {
          console.log(err);
          return;
        }
        return res.json({
          success: 1,
          data: results,
          message: "Product created successfully"
        });
      });
    },
    removeProductById: (req, res) => {
      const id = req.params.product_id;
      removeProductById(id, (err, results) => {
        if (err) {
          console.log(err);
          return;
        }
        return res.json({
          success: 1,
          data: results
        });
      });
    },
    getProductByProductId: (req, res) => {
      const id = req.params.id;
      getProductByProductId(id, (err, results) => {
        if (err) {
          console.log(err);
          return;
        }
        if (!results) {
          return res.json({
            success: 0,
            message: "Record not Found"
          });
        }
        results.password = undefined;
        return res.json({
          success: 1,
          data: results
        });
      });
    },
    getProducts: (req, res) => {
      getProducts((err, results) => {
        if (err) {
          console.log(err);
          return;
        }
        return res.json({
          success: 1,
          data: results
        });
      });
    },
    updateProductQuantity: (req, res) => {
      const body = req.body;
      updateProductQuantity(body, (err, results) => {
        if (err) {
          console.log(err);
          return;
        }
        return res.json({
          success: 1,
          message: "updated successfully"
        });
      });
    },
    updateProduct: (req, res) => {
      const body = req.body;
      updateProduct(body, (err, results) => {
        if (err) {
          console.log(err);
          return;
        }
        return res.json({
          success: 1,
          message: "updated successfully"
        });
      });
    }
  };
  