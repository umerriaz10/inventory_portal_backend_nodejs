const router = require("express").Router();
const { checkToken } = require("../../auth/token_validation");
const {
  getProductByProductId,
  getProducts,
  updateProductQuantity,
  updateProduct,
  postProduct,
  removeProductById
} = require("./product.controller");

router.post("/", checkToken, postProduct);
router.get("/", checkToken, getProducts);
router.get("/:id", checkToken, getProductByProductId);
router.delete("/:product_id", checkToken, removeProductById);
router.patch("/updateQuantity", checkToken, updateProductQuantity);
router.patch("/", checkToken, updateProduct);

module.exports = router;
