const pool = require("../../config/database");

module.exports = {
  postPayment: (data, callBack) => {
    pool.query(
        `insert into payments(vendor_id, vendor_name, order_id, amount, pay, receive, addedBy) 
                  values(?,?,?,?,?,?,?)`,
        [
          data.vendor_id,
          data.vendor_name,
          data.order_id,
          data.amount,
          data.pay,
          data.receive,
          data.addedBy
        ],
        (error, results, fields) => {
          if (error) {
            callBack(error);
          }
          return callBack(null, results);
        }
      );
  },
  removePaymentById: (id, callBack) => {
    pool.query(
      `delete from payments where id = ?`,
      [id],
      (error, results, fields) => {
        if (error) {
          callBack(error);
        }
        return callBack(null, results);
      }
    );
  },
getPaymentsById: (id, callBack) => {
    pool.query(
      `select id,vendor_id, vendor_name, order_id, amount, pay, receive, addedBy from payments where vendor_id = ?`,
      [id],
      (error, results, fields) => {
        if (error) {
          callBack(error);
        }
        return callBack(null, results);
      }
    );
  },
  getPayments: callBack => {
    pool.query(
      `select id,vendor_id, vendor_name, order_id, amount, pay, receive, addedBy from payments`,
      [],
      (error, results, fields) => {
        if (error) {
          callBack(error);
        }
        return callBack(null, results);
      }
    );
  },
//   updateProductQuantity: (data, callBack) => {
//     var sign = '';
//     if (data.status == 'Added') {
//       sign = '+';
//     } else {
//       sign = '-';
//     }
//     pool.query(
//       `update products set p_quantity = p_quantity ` + sign + `? where id = ?`,
//       [
//         data.p_quantity,
//         data.id
//       ],
//       (error, results, fields) => {
//         if (error) {
//           callBack(error);
//         }
//         return callBack(null, results[0]);
//       }
//     );
//   },
//   updateProduct: (data, callBack) => {
//     pool.query(
//       `update products set
//         p_name = ?, p_quantity = ?,p_price =? where id = ?`,
//       [
//         data.p_name,
//         data.p_quantity,
//         data.p_price,
//         data.id
//       ],
//       (error, results, fields) => {
//         if (error) {
//           callBack(error);
//         }
//         return callBack(null, results[0]);
//       }
//     );
//   }
};
