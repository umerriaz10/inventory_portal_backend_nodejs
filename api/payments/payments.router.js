const router = require("express").Router();
const { checkToken } = require("../../auth/token_validation");
const {
    getPaymentsById,
  getPayments,
  updateProductQuantity,
  updateProduct,
  postPayment,
  removePaymentById
} = require("./payments.controller");

router.post("/", checkToken, postPayment);
router.get("/", checkToken, getPayments);
router.get("/:id", checkToken, getPaymentsById);
// router.get("/:id", checkToken, getProductByProductId);
router.delete("/:payment_id", checkToken, removePaymentById);
// router.patch("/updateQuantity", checkToken, updateProductQuantity);
// router.patch("/", checkToken, updateProduct);

module.exports = router;
