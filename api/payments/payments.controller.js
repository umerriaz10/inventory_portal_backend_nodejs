const {
    getPaymentsById,
    getPayments,
    updateProduct,
    updateProductQuantity,
    postPayment,
    removePaymentById
  } = require("./payments.service");
  
  
  module.exports = {
    postPayment: (req, res) => {
      const body = req.body;
      postPayment(body, (err, results) => {
        if (err) {
          console.log(err);
          return;
        }
        return res.json({
          success: 1,
          data: results,
          message: "Payment created successfully"
        });
      });
    },
    removePaymentById: (req, res) => {
      const id = req.params.payment_id;
      removePaymentById(id, (err, results) => {
        if (err) {
          console.log(err);
          return;
        }
        return res.json({
          success: 1,
          data: results
        });
      });
    },
    getPaymentsById: (req, res) => {
      const id = req.params.id;
      getPaymentsById(id, (err, results) => {
        if (err) {
          console.log(err);
          return;
        }
        if (!results) {
          return res.json({
            success: 0,
            message: "Record not Found"
          });
        }
        results.password = undefined;
        return res.json({
          success: 1,
          data: results
        });
      });
    },
    getPayments: (req, res) => {
        getPayments((err, results) => {
        if (err) {
          console.log(err);
          return;
        }
        return res.json({
          success: 1,
          data: results
        });
      });
    },
    // updateProductQuantity: (req, res) => {
    //   const body = req.body;
    //   updateProductQuantity(body, (err, results) => {
    //     if (err) {
    //       console.log(err);
    //       return;
    //     }
    //     return res.json({
    //       success: 1,
    //       message: "updated successfully"
    //     });
    //   });
    // },
    // updateProduct: (req, res) => {
    //   const body = req.body;
    //   updateProduct(body, (err, results) => {
    //     if (err) {
    //       console.log(err);
    //       return;
    //     }
    //     return res.json({
    //       success: 1,
    //       message: "updated successfully"
    //     });
    //   });
    // }
  };
  