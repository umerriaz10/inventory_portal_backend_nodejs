const pool = require("../../config/database");

module.exports = {
  postOrder: (data, callBack) => {
    pool.query(
        `insert into orders(vendor_id, vendor_name, no_of_products, date, city, status, order_type, orderedBy, total_amount) 
                  values(?,?,?,?,?,?,?,?,?)`,
        [
          data.vendor_id,
          data.vendor_name,
          data.no_of_products,
          data.date,
          data.city,
          data.status,
          data.order_type,
          data.orderedBy,
          data.total_amount
        ],
        (error, results, fields) => {
          if (error) {
            return callBack(error);
          }
          return callBack(null, results);
        }
      );
  },
  getOrders: callBack => {
    pool.query(
      `select id,vendor_id,vendor_name,no_of_products,date,city,status,order_type,orderedBy,modifiedBy,total_amount from orders`,
      [],
      (error, results, fields) => {
        if (error) {
          callBack(error);
        }
        return callBack(null, results);
      }
    );
  },
  getOrderById: (id, callBack) => {
    pool.query(
      `select id,vendor_id,vendor_name,no_of_products,date,city,status,order_type,orderedBy,modifiedBy,total_amount from orders where id = ?`,
      [id],
      (error, results, fields) => {
        if (error) {
          callBack(error);
        }
        return callBack(null, results);
      }
    );
  },
  getOrderByVendorId: (id, callBack) => {
    pool.query(
      `select id,vendor_id,vendor_name,no_of_products,date,city,status,order_type,orderedBy,modifiedBy,total_amount from orders where vendor_id = ?`,
      [id],
      (error, results, fields) => {
        if (error) {
          callBack(error);
        }
        return callBack(null, results);
      }
    );
  },
  removeOrderById: (id, callBack) => {
    pool.query(
      `delete from orders where id = ?`,
      [id],
      (error, results, fields) => {
        if (error) {
          callBack(error);
        }
        return callBack(null, results);
      }
    );
  },
  updateOrder: (data, callBack) => {
    pool.query(
      `update orders set
      vendor_id = ?, vendor_name = ?, no_of_products = ?, date = ?,city = ?, status = ?, order_type = ?, modifiedBy = ?, total_amount = ? where id = ?`,
      [
        data.vendor_id,
        data.vendor_name,
        data.no_of_products,
        data.date,
        data.city,
        data.status,
        data.order_type,
        data.modifiedBy,
        data.total_amount,
        data.id
      ],
      (error, results, fields) => {
        if (error) {
          callBack(error);
        }
        return callBack(null, results[0]);
      }
    );
  }
};
