const router = require("express").Router();
const { checkToken } = require("../../auth/token_validation");
const {
  postOrder, getOrders, getOrderById, removeOrderById, updateOrder, getOrderByVendorId
} = require("./order.controller");

router.post("/", checkToken, postOrder);
router.get("/", checkToken, getOrders);
router.get("/:id", checkToken, getOrderById);
router.get("/:id/:vendor_id", checkToken, getOrderByVendorId);
router.delete("/:order_id", checkToken, removeOrderById);
router.patch("/", checkToken, updateOrder);

module.exports = router;