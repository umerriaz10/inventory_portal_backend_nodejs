const {
    postOrder, getOrders, getOrderById, removeOrderById, updateOrder, getOrderByVendorId
  } = require("./order.service");
  
  
  module.exports = {
    postOrder: (req, res, next) => {
      const body = req.body;
      postOrder(body, (err, results) => {
        if (err) {
          return next.json(err);
        }
        return res.json({
          success: 1,
          data: results,
          message: "Order created successfully"
        });
      });
    },
    getOrders: (req, res) => {
      getOrders((err, results) => {
        if (err) {
          console.log(err);
          return;
        }
        return res.json({
          success: 1,
          data: results
        });
      });
    },
    getOrderById: (req, res) => {
      const id = req.params.id;
      getOrderById(id, (err, results) => {
        if (err) {
          console.log(err);
          return;
        }
        return res.json({
          success: 1,
          data: results
        });
      });
    },
    getOrderByVendorId: (req, res) => {
      const id = req.params.vendor_id;
      getOrderByVendorId(id, (err, results) => {
        if (err) {
          console.log(err);
          return;
        }
        return res.json({
          success: 1,
          data: results
        });
      });
    },
    removeOrderById: (req, res) => {
      const id = req.params.order_id;
      removeOrderById(id, (err, results) => {
        if (err) {
          console.log(err);
          return;
        }
        return res.json({
          success: 1,
          data: results
        });
      });
    },
    updateOrder: (req, res) => {
      const body = req.body;
      updateOrder(body, (err, results) => {
        if (err) {
          console.log(err);
          return;
        }
        return res.json({
          success: 1,
          message: "updated successfully"
        });
      });
    }
  };
  

