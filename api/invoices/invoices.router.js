const router = require("express").Router();
const { checkToken } = require("../../auth/token_validation");
const {
    getInvoiceByInvoiceId,
    postInvoice,
    getInvoices,
    updateInvoice
} = require("./invoices.controller");

router.post("/", checkToken, postInvoice);
router.get("/", checkToken, getInvoices);
router.get("/:id", checkToken, getInvoiceByInvoiceId);
router.patch("/", checkToken, updateInvoice);

module.exports = router;
