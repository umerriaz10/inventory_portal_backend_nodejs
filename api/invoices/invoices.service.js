const pool = require("../../config/database");

module.exports = {
    postInvoice: (data, callBack) => {
        pool.query(
            `insert into invoices(order_id, vendor_id, vendor_name, date, created_by, total_amount, status) 
                      VALUES ?`,
            [data],
            (error, results, fields) => {
              if (error) {
                callBack(error);
              }
              return callBack(null, results);
            }
          );
    },
    getInvoiceByInvoiceId: (id, callBack) => {
    pool.query(
      `select id,order_id, vendor_id, vendor_name, date, created_by, total_amount, status from invoices where id = ?`,
      [id],
      (error, results, fields) => {
        if (error) {
          callBack(error);
        }
        return callBack(null, results[0]);
      }
    );
  },
  getInvoices: callBack => {
    pool.query(
      `select id,order_id, vendor_id, vendor_name, date, created_by, total_amount, status from invoices`,
      [],
      (error, results, fields) => {
        if (error) {
          callBack(error);
        }
        return callBack(null, results);
      }
    );
  },
  updateInvoices: (data, callBack) => {
    pool.query(
      `update invoices set p_quantity = p_quantity ` + sign + `? where id = ?`,
      [
        data.p_quantity,
        data.id
      ],
      (error, results, fields) => {
        if (error) {
          callBack(error);
        }
        return callBack(null, results[0]);
      }
    );
  }
};
