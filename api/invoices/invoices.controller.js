const {
    getInvoiceByInvoiceId,
    postInvoice,
    getInvoices,
    updateInvoice
  } = require("./invoices.service");
  
  
  module.exports = {
    postInvoice: (req, res) => {
        const body = req.body;
        postInvoice(body, (err, results) => {
          if (err) {
            console.log(err);
            return;
          }
          return res.json({
            success: 1,
            message: "Invoice created successfully"
          });
        });
      },
    getInvoiceByInvoiceId: (req, res) => {
      const id = req.params.id;
      getInvoiceByInvoiceId(id, (err, results) => {
        if (err) {
          console.log(err);
          return;
        }
        if (!results) {
          return res.json({
            success: 0,
            message: "Record not Found"
          });
        }
        results.password = undefined;
        return res.json({
          success: 1,
          data: results
        });
      });
    },
    getInvoices: (req, res) => {
      getInvoices((err, results) => {
        if (err) {
          console.log(err);
          return;
        }
        return res.json({
          success: 1,
          data: results
        });
      });
    },
    updateInvoice: (req, res) => {
      const body = req.body;
      updateInvoice(body, (err, results) => {
        if (err) {
          console.log(err);
          return;
        }
        return res.json({
          success: 1,
          message: "updated successfully"
        });
      });
    }
  };
  