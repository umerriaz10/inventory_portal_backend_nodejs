const {
    createInvRec,
    getInvRec
  } = require("./invRec.service");
  
  module.exports = {
    createInvRec: (req, res) => {
      const body = req.body;
      createInvRec(body, (err, results) => {
        if (err) {
          console.log(err);
          return res.status(500).json({
            success: 0,
            message: err
          });
        }
        return res.status(200).json({
          success: 1,
          data: results,
          message: "InventoryRecord created successfully"
        });
      });
    },
    getInvRec: (req, res) => {
      getInvRec((err, results) => {
        if (err) {
          console.log(err);
          return;
        }
        return res.json({
          success: 1,
          data: results
        });
      });
    }
  };
  