const pool = require("../../config/database");

module.exports = {
  createInvRec: (data, callBack) => {
    pool.query(
      `insert into invRec(date, no_of_products, status, addedBy) 
                values(?,?,?,?)`,
      [
        data.date,
        data.no_of_products,
        data.status,
        data.addedBy
      ],
      (error, results, fields) => {
        if (error) {
          callBack(error);
        }
        return callBack(null, results);
      }
    );
  },
  getInvRec: callBack => {
    pool.query(
      `select id,date,no_of_products,status,addedBy from invRec`,
      [],
      (error, results, fields) => {
        if (error) {
          callBack(error);
        }
        return callBack(null, results);
      }
    );
  }
};
