const router = require("express").Router();
const { checkToken } = require("../../auth/token_validation");
const {
  createInvRec,
  getInvRec
} = require("./invRec.controller");

router.get("/", checkToken, getInvRec);
router.post("/", checkToken, createInvRec);

module.exports = router;
