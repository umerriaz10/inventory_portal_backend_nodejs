const pool = require("../../config/database");

module.exports = {
    postInvRecItem: (data, callBack) => {
    pool.query(
        `insert into invRec_items(invRec_id, item_id, item_name, quantity) 
                  VALUES ?`,
        [data],
        (error, results, fields) => {
          if (error) {
            callBack(error);
          }
          return callBack(null, results);
        }
      );
  },
  getInvRecById: (id, callBack) => {
    pool.query(
      `select item_name, quantity from invRec_items where invRec_id = ?`,
      [id],
      (error, results, fields) => {
        if (error) {
          callBack(error);
        }
        return callBack(null, results);
      }
    );
  }
};
