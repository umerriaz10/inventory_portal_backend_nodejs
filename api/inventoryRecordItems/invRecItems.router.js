const router = require("express").Router();
const { checkToken } = require("../../auth/token_validation");
const {
    postInvRecItem,
    getInvRecById
} = require("./invRecItems.controller");

router.post("/", checkToken, postInvRecItem);
router.get("/:id", checkToken, getInvRecById);

module.exports = router;
