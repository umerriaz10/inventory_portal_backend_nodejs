const {
    postInvRecItem,
    getInvRecById
  } = require("./invRecItems.service");
  
  
  module.exports = {
    postInvRecItem: (req, res) => {
      const body = req.body;
      postInvRecItem(body, (err, results) => {
        if (err) {
          console.log(err);
          return;
        }
        return res.json({
          success: 1,
          message: "Order Item created successfully"
        });
      });
    },
    getInvRecById: (req, res) => {
      const id = req.params.id;
      getInvRecById(id, (err, results) => {
        if (err) {
          console.log(err);
          return;
        }
        return res.json({
          success: 1,
          data: results
        });
      });
    }
  };
  