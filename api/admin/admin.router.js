const router = require("express").Router();
const {
  login,
  createAdmin
} = require("./admin.controller");

router.post("/login", login);
router.post("/", createAdmin);

module.exports = router;
