const pool = require("../../config/database");

module.exports = {
  postVendor: (data, callBack) => {
    pool.query(
        `insert into vendors(v_name, v_email, v_contact_number, v_address, v_balance, v_totalPaid, v_paidToVendor, v_addedBy) 
                  values(?,?,?,?,?,?,?,?)`,
        [
          data.v_name,
          data.v_email,
          data.v_contact_number,
          data.v_address,
          data.v_balance,
          data.v_totalPaid,
          data.v_paidToVendor,
          data.v_addedBy
        ],
        (error, results, fields) => {
          if (error) {
            callBack(error);
          }
          return callBack(null, results);
        }
      );
  },
  getVendors: callBack => {
    pool.query(
      `select id,v_name,v_email,v_contact_number,v_address,v_balance,v_paidToVendor,v_addedBy,v_totalPaid from vendors`,
      [],
      (error, results, fields) => {
        if (error) {
          callBack(error);
        }
        return callBack(null, results);
      }
    );
  },
  getVendorById: (id, callBack) => {
    pool.query(
      `select id,v_name,v_email,v_contact_number,v_address,v_balance,v_paidToVendor,v_addedBy,v_totalPaid from vendors where id = ?`,
      [id],
      (error, results, fields) => {
        if (error) {
          callBack(error);
        }
        return callBack(null, results);
      }
    );
  },
  updateVendorBalance: (data, callBack) => {
    var sign = '';
    if (data.status == 'Added') {
      sign = '+';
    } else {
      sign = '-';
    }
    pool.query(
      `update vendors set v_balance = v_balance ` + sign + `? where id = ?`,
      [
        data.v_balance,
        data.id
      ],
      (error, results, fields) => {
        if (error) {
          callBack(error);
        }
        return callBack(null, results[0]);
      }
    );
  },
  updateVendor: (data, callBack) => {
    pool.query(
      `update vendors set
      v_name = ?, v_email = ?, v_contact_number = ?, v_address = ?,v_balance = ?, v_addedBy = ?, v_totalPaid = ?, v_paidToVendor = ?, v_modifiedBy = ? where id = ?`,
      [
        data.v_name,
        data.v_email,
        data.v_contact_number,
        data.v_address,
        data.v_balance,
        data.v_addedBy,
        data.v_totalPaid,
        data.v_paidToVendor,
        data.v_modifiedBy,
        data.id
      ],
      (error, results, fields) => {
        if (error) {
          callBack(error);
        }
        return callBack(null, results[0]);
      }
    );
  }
};
