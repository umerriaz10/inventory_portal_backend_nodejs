const {
    getVendors, postVendor, updateVendorBalance, getVendorById, updateVendor
  } = require("./vendor.service");
  
  
  module.exports = {
    postVendor: (req, res) => {
      const body = req.body;
      postVendor(body, (err, results) => {
        if (err) {
          console.log(err);
          return;
        }
        return res.json({
          success: 1,
          data: results,
          message: "Vendor created successfully"
        });
      });
    },
    getVendors: (req, res) => {
        getVendors((err, results) => {
        if (err) {
          console.log(err);
          return;
        }
        return res.json({
          success: 1,
          data: results
        });
      });
    },
    getVendorById: (req, res) => {
      const id = req.params.id;
      getVendorById(id, (err, results) => {
        if (err) {
          console.log(err);
          return;
        }
        return res.json({
          success: 1,
          data: results
        });
      });
    },
    updateVendorBalance: (req, res) => {
      const body = req.body;
      updateVendorBalance(body, (err, results) => {
        if (err) {
          console.log(err);
          return;
        }
        return res.json({
          success: 1,
          message: "updated successfully"
        });
      });
    },
    updateVendor: (req, res) => {
      const body = req.body;
      updateVendor(body, (err, results) => {
        if (err) {
          console.log(err);
          return;
        }
        return res.json({
          success: 1,
          message: "updated successfully"
        });
      });
    }
  };
  