const router = require("express").Router();
const { checkToken } = require("../../auth/token_validation");
const {
  getVendors, postVendor, updateVendorBalance, getVendorById, updateVendor
} = require("./vendor.controller");

router.post("/", checkToken, postVendor);
router.get("/", checkToken, getVendors);
router.get("/:id", checkToken, getVendorById);
router.patch("/", checkToken, updateVendor);
router.patch("/updateBalance", checkToken, updateVendorBalance);

module.exports = router;
