require("dotenv").config();
const express = require("express");
const app = express();
const userRouter = require("./api/users/user.router");
const adminRouter = require("./api/admin/admin.router");
const productsRouter = require("./api/products/products.router");
const invRecRouter = require("./api/inventoryRecords/invRec.router");
const vendorRouter = require("./api/vendors/vendor.router");
const orderRouter = require("./api/order/order.router");
const orderItemRouter = require("./api/orderItem/orderItem.router");
const invRecItemsRouter = require("./api/inventoryRecordItems/invRecItems.router");
const invoiceRouter = require("./api/invoices/invoices.router");
const paymentRouter = require("./api/payments/payments.router");

app.use(express.json());

app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE,PATCH,OPTIONS');
  res.header('Access-Control-Allow-Credentials', 'true');
  res.setHeader('Access-Control-Allow-Headers', 'Access-Control-Allow-Headers, Origin,Accept, X-Requested-With, Content-Type, Access-Control-Request-Method, Access-Control-Request-Headers,X-Access-Token,XKey,Authorization');
  next();
});
app.options("/*", function(req, res, next) {
  res.header('Access-Control-Allow-Origin', '*');
  res.header('Access-Control-Allow-Methods', 'GET,PUT,PATCH,POST,DELETE,OPTIONS');
  res.header('Access-Control-Allow-Headers', 'Content-Type, Authorization, Content-Length, X-Requested-With');
  res.sendStatus(200);
});

app.use("/api/users", userRouter);
app.use("/api/admin", adminRouter);
app.use("/api/products", productsRouter);
app.use("/api/inventoryRecord", invRecRouter);
app.use("/api/inventoryRecordItems", invRecItemsRouter);
app.use("/api/vendors", vendorRouter);
app.use("/api/order", orderRouter);
app.use("/api/orderItem", orderItemRouter);
app.use("/api/invoice", invoiceRouter);
app.use("/api/payments", paymentRouter);

const port = process.env.PORT;
app.listen(port, () => {
  console.log("server up and running on PORT :", port);
});
